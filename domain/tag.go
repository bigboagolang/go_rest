package domain

type Tag struct {
	Id    string `json:"id,omitempty"`
	Value string `json:"value,omitempty"`
}

type Tags []Tag
