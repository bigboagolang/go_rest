package test_cases

import (
	"errors"
	"fmt"
	"gitlab.com/NikolayZhurbin/go_rest/consts"
	"gitlab.com/NikolayZhurbin/go_rest/task-service"
	"gitlab.com/NikolayZhurbin/go_rest/token"
	"gitlab.com/NikolayZhurbin/go_rest/user-service"
	"log"
	"math/rand"
	"strconv"
	"time"
)

var (
	uInfo *user_service.UserInfo
	t     *token.AccessToken
)

//Create new user and get token and user info
func TaskE2EInit(prt bool) {
	var err error
	s := rand.NewSource(time.Now().UnixNano())
	rNum := rand.New(s)

	num := strconv.Itoa(rNum.Int())
	t, err = user_service.CreateUserAndGetToken(
		"test"+num+"@test.ru",
		"First"+num+"Name",
		"Last"+num+"Name",
		"Middle"+num+"Name",
		"1234",
		"test"+num+"User",
		"1234",
	)
	if err != nil {
		log.Fatalln(err)
	}
	if prt {
		fmt.Println("-TaskE2EInit")
		fmt.Printf("---User Name: %v\n", "test"+num+"User")
		fmt.Printf("---Access Token: %v\n", *t)
	}
	uInfo = new(user_service.UserInfo)
	err = uInfo.GetUserByToken(consts.Client, t.Token)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	if prt {
		fmt.Printf("---User Info: %v\n", *uInfo)
	}
}

//Create new task
func createTask(ptr bool) (task *task_service.Task, err error) {
	if ptr {
		fmt.Println("-Creating Task")
	}
	if uInfo == nil {
		return nil, errors.New("uInfo is nil. Need to execute TaskE2EInit")
	}
	s := rand.NewSource(time.Now().UnixNano())
	rNum := rand.New(s)
	comment := task_service.Comment{
		AssignedTo: uInfo.AccountId,
		DueDate:    "2020-12-31",
		Priority:   task_service.Priority{Id: "ed8906d0-ea68-11e9-8124-2e6b45b29a25"},
		Status:     task_service.Status{Id: "ed7dfcb8-ea68-11e9-8124-2e6b45b29a25"},
		Subject:    "createTask" + strconv.Itoa(rNum.Int()),
		Text:       "createTask" + strconv.Itoa(rNum.Int()),
	}
	err = comment.CreateComment(t.Token)
	if err != nil {
		log.Printf("Error during CreateComment: %v", err)
		return nil, err
	}
	tasks, err := task_service.GetAllTasks(consts.Client, t.Token)
	if err != nil {
		return nil, errors.New("tasks count != n+1")
	}
	if len(*tasks) > 1 {
		return nil, errors.New("tasks count > 1")
	}
	for _, v := range *tasks {
		task = &v
	}
	if task == nil {
		return nil, errors.New("task is nil")
	}
	return task, nil
}

//Create n new tasks
func CreateNTasks(n int) error {
	if uInfo == nil {
		return errors.New("uInfo is nil. Need to execute TaskE2EInit")
	}
	return nil
}

//Create one task with n comments
func CreateTaskWithNComments(n int) {

}

//Create one comment under specified task
func AddCommentToTask(tid string) {

}

//Create N comments under specified task
func AddNCommentsToNewTask(n int, tid string) {

}

//Create N new users in one new group. Create new task and distribute it on created group.
// Create achievement for every created user. Tag all created objects.
func DistributeNewTaskToNewGroupWithTags(n int, prt bool, tags string) error {
	if uInfo == nil {
		return errors.New("uInfo is nil. Need to execute TaskE2EInit")
	}
	s := rand.NewSource(time.Now().UnixNano())
	rNum := rand.New(s)
	num := strconv.Itoa(rNum.Int())
	gId := user_service.CreateNUsersWithSkillsInOneGroup(t, n, prt, tags)
	if prt {
		fmt.Printf("-Created N-Users In One Group, gId: %v\n", gId)
	}

	err := user_service.CreateActivityWithTagToGroup(t, num, tags, gId)
	if err != nil {
		log.Printf("Error during CreateActivityWithTagToGroup: %v\n", err)
		return err
	}
	task, err := createTask(prt)
	if err != nil {
		log.Printf("Error during createTask: %v\n", err)
		return err
	}
	err = task.TagTask(consts.Client, t.Token, tags)
	if err != nil {
		log.Printf("Error during TagTask: %v\n", err)
		return err
	}
	if prt {
		fmt.Printf("-Tag task with id: %v by tags: %v\n", task.Id, tags)
	}
	gIds := make([]string, 0)
	gIds = append(gIds, gId)
	err = task.DistributeTask(consts.Client, t.Token, &gIds, prt)
	if err != nil {
		log.Printf("Error during DistributeTask: %v\n", err)
		return err
	}
	tasks, err := task_service.GetAllTasks(consts.Client, t.Token)
	if len(*tasks) != n+1 {
		return errors.New("tasks count != n+1")
	}
	if prt {
		for _, v := range *tasks {
			v.Print()
		}
	}
	return nil
}
