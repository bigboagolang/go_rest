package helper

import (
	"net/http"
	"strings"
)

const (
	boundary = "boundary"
)

type FormData struct {
	Key   string
	Value string
}

type Header struct {
	Key   string
	Value string
}

func DoPost(url string, payload *strings.Reader, h *[]Header) (*http.Response, error) {
	req, err := http.NewRequest("POST", url, payload)
	if err != nil {
		return nil, err
	}

	l := len(*h)
	for i := 0; i < l; i++ {
		req.Header.Add((*h)[i].Key, (*h)[i].Value)
	}
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func DoPostFormData(url string, f *[]FormData, token string) (*http.Response, error) {
	h := []Header{
		{Key: "content-type", Value: "multipart/form-data; boundary=boundary"},
		{Key: "Authorization", Value: "Bearer " + token},
		{Key: "Content-Type", Value: "application/x-www-form-urlencoded"},
		//		{Key:"Accept", Value: "*/*"},
		//		{Key:"Cache-Control", Value: "no-cache"},
		//		{Key:"Accept-Encoding", Value: "gzip, deflate"},
		//		{Key:"Connection", Value: "keep-alive"},
		//		{Key:"cache-control", Value: "no-cache"},
	}
	bound := "\r\n--" + boundary + "\r\nContent-Disposition: form-data; name=\""
	s := "--" + boundary + "\r\nContent-Disposition: form-data; name=\""

	l := len(*f)
	for i := 0; i < l; i++ {
		s += (*f)[i].Key
		s += "\"\r\n\r\n"
		s += (*f)[i].Value
		if i == l-1 {
			s += "\r\n--" + boundary + "--"
		} else {
			s += bound
		}
	}
	payload := strings.NewReader(s)

	res, err := DoPost(url, payload, &h)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func DoPatch(url string, payload *strings.Reader, h *[]Header) (*http.Response, error) {
	req, err := http.NewRequest("PATCH", url, payload)
	if err != nil {
		return nil, err
	}

	l := len(*h)
	for i := 0; i < l; i++ {
		req.Header.Add((*h)[i].Key, (*h)[i].Value)
	}
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func DoPatchFormData(url string, f *[]FormData, token string) (*http.Response, error) {
	h := []Header{
		{Key: "content-type", Value: "multipart/form-data; boundary=boundary"},
		{Key: "Authorization", Value: "Bearer " + token},
		{Key: "Content-Type", Value: "application/x-www-form-urlencoded"},
		//		{Key:"Accept", Value: "*/*"},
		//		{Key:"Cache-Control", Value: "no-cache"},
		//		{Key:"Accept-Encoding", Value: "gzip, deflate"},
		//		{Key:"Connection", Value: "keep-alive"},
		//		{Key:"cache-control", Value: "no-cache"},
	}
	bound := "\r\n--" + boundary + "\r\nContent-Disposition: form-data; name=\""
	s := "--" + boundary + "\r\nContent-Disposition: form-data; name=\""

	l := len(*f)
	for i := 0; i < l; i++ {
		s += (*f)[i].Key
		s += "\"\r\n\r\n"
		s += (*f)[i].Value
		if i == l-1 {
			s += "\r\n--" + boundary + "--"
		} else {
			s += bound
		}
	}
	payload := strings.NewReader(s)

	res, err := DoPatch(url, payload, &h)
	if err != nil {
		return nil, err
	}
	return res, nil
}
