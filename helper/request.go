package helper

import (
	"bytes"
	"io/ioutil"
	"net/http"
)

func HttpRequest(headerReq map[string]string, bodyReq []byte, url, method string) ([]byte, error) {
	payload := bytes.NewReader(bodyReq)
	req, err := http.NewRequest(method, url, payload)
	if err != nil {
		return nil, err
	}
	for k, v := range headerReq {
		req.Header.Add(k, v)
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	return body, nil
}
