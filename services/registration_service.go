package services

import (
	"encoding/json"
	"errors"
	"github.com/go-resty/resty"
	"gitlab.com/NikolayZhurbin/go_rest/consts"
	"gitlab.com/NikolayZhurbin/go_rest/helper"

	//"go_rest/helper"
	"log"
)

type SelfRegistration struct {
	Email      string `json:"email,omitempty"`
	FirstName  string `json:"firstName,omitempty"`
	LastName   string `json:"lastName,omitempty"`
	MiddleName string `json:"middleName,omitempty"`
	Password   string `json:"password,omitempty"`
	Phone      string `json:"phone,omitempty"`
	UserName   string `json:"userName,omitempty"`
}

func CreateSelfRegistration(Email, FirstName, LastName, MiddleName, Phone, UserName, Password string) *SelfRegistration {
	r := SelfRegistration{
		UserName:   UserName,
		Password:   Password,
		FirstName:  FirstName,
		LastName:   LastName,
		Email:      Email,
		MiddleName: MiddleName,
		Phone:      Phone,
	}
	return &r
}

func (s *SelfRegistration) CreateUser(c *resty.Client) error {

	body, err := json.Marshal(*s)
	if err != nil {
		log.Printf("Error while Marshal: %v", err)
		return err
	}
	resp, err := c.R().
		SetHeader("Content-Type", "application/json").
		SetBody(body).
		Post(consts.RegistrationServiceUrl)
	if !helper.CheckResponse(resp, err) {
		return errors.New("CreateUser CheckResponse failed")
	}
	if err != nil {
		log.Printf("Error while user registration: %v", err)
		return err
	}

	return nil
}

func (s *SelfRegistration) DeleteUser(c *resty.Client, id, token string) error {
	_, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"accountId": id}).
		Delete(consts.RegistrationServiceUrl + "{accountId}")
	if err != nil {
		log.Printf("Error while user registration: %v", err)
		return err
	}
	return nil
}
