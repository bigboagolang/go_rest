package services

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-resty/resty"
	"gitlab.com/NikolayZhurbin/go_rest/consts"
	"gitlab.com/NikolayZhurbin/go_rest/helper"
	"log"
	"strconv"
)

//!
func GetAllActivities(c *resty.Client, token string) (*[]Activity, error) {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		Get(consts.GetAllActivitiesUrl)

	if !helper.CheckResponse(resp, err) {
		return nil, errors.New("GetAllActivities CheckResponse failed")
	}

	res := make([]Activity, 0)
	err = json.Unmarshal(resp.Body(), &res)
	if err != nil {
		log.Printf("Error while unmarshal GetAllActivities: %v", err)
		return nil, err
	}
	return &res, nil
}

//!
func PrintAllActivities(a *[]Activity, withSchedule bool) {
	for _, v := range *a {
		v.PrintActivity(withSchedule)
	}
}

//!
func (a *Activity) PrintActivity(withSchedule bool) {
	fmt.Println("Printing Activity")
	fmt.Printf("ID: %v\n", a.Id)
	fmt.Printf("Subject: %v\n", a.Subject)
	fmt.Printf("Description: %v\n", a.Description)
	fmt.Printf("ActivityTime: %v\n", a.ActivityTime)
	fmt.Printf("Cancelled: %v\n", a.Cancelled)
	fmt.Printf("CreatedById: %v\n", a.CreatedById)
	fmt.Printf("CreatedByName: %v\n", a.CreatedByName)
	fmt.Printf("CurrentUserStatus: %v\n", a.CurrentUserStatus)
	fmt.Printf("StartDate: %v\n", a.StartDate)
	fmt.Printf("EndDate: %v\n", a.EndDate)
	fmt.Printf("Repeated: %v\n", a.Repeated)
	fmt.Printf("Updated: %v\n", a.Updated)
	fmt.Printf("Tags: %v\n", a.Tags)
	fmt.Printf("Participants: %v\n", a.Participants)
	if withSchedule {
		for _, v := range a.Schedules {
			v.PrintSchedule()
		}

	}
	fmt.Println(consts.PrintHeader)
}

func (s *Schedule) PrintSchedule() {
	fmt.Println("Printing Schedule")
	fmt.Printf("ID: %v\n", s.Id)
	fmt.Printf("Description: %v\n", s.Description)
	fmt.Printf("CurrentUserStatus: %v\n", s.CurrentUserStatus)
	fmt.Printf("StartTime: %v\n", s.StartTime)
	fmt.Printf("EndTime: %v\n", s.EndTime)
	fmt.Printf("OldTime: %v\n", s.OldTime)
	fmt.Printf("Updated: %v\n", s.Updated)
	fmt.Printf("Cancelled: %v\n", s.Cancelled)
	fmt.Printf("Participants: %v\n", s.Participants)
	fmt.Println(consts.PrintHeader)
}

//!
func (a *Activity) CreateActivity(c *resty.Client, token string) error {
	formData := []helper.FormData{
		{Key: "activityTime", Value: a.ActivityTime},
		{Key: "description", Value: a.Description},
		{Key: "duration.hours", Value: strconv.Itoa(a.Duration.Hours)},
		{Key: "subject", Value: a.Subject},
		{Key: "repeated", Value: strconv.FormatBool(a.Repeated)},
		{Key: "repeatInterval.days", Value: strconv.Itoa(a.RepeatInterval.Days)},
	}
	if len(a.Participants) > 0 {
		if len(a.Participants[0].AccountId) > 0 {
			p := helper.FormData{
				Key:   "participants[0].accountId",
				Value: a.Participants[0].AccountId,
			}
			formData = append(formData, p)
		}
		if len(a.Participants[0].GroupId) > 0 {
			p := helper.FormData{
				Key:   "participants[0].groupId",
				Value: a.Participants[0].GroupId,
			}
			formData = append(formData, p)
		}
	}

	url := consts.CreateActivityUrl
	resp, err := helper.DoPostFormData(url, &formData, token)
	if !helper.CheckRawResponse(resp, err) {
		return errors.New("CreateActivity CheckResponse failed")
	}
	byteNum := int64(1000000)
	if resp.ContentLength > 0 {
		byteNum = resp.ContentLength
	}
	b := make([]byte, byteNum)

	n, err := resp.Body.Read(b)
	if err != nil {
		log.Printf("Error while read boby: %v", err)
		return err
	}
	if resp.ContentLength == -1 {
		b = b[0:n]
	}
	err = json.Unmarshal(b, a)
	if err != nil {
		log.Printf("Error while unmarshal CreateActivity: %v", err)
		return err
	}
	return nil
}

//!
func GetActivitiesForPeriod(c *resty.Client, token, start, end string) (a *[]Activity, err error) {
	bodyReq, err := json.Marshal(ActivitiesForPeriod{
		StartDate: start,
		EndDate:   end,
	})
	if err != nil {
		log.Printf("Error while Marshal: %v", err)
		return nil, err
	}
	headers := map[string]string{
		"Authorization": "Bearer eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJpOFV2cWd3T1d4NWl1RFp0ZFRyUldOUlpoVnB0VGxtR2Y1OXBwWGZsaDhvIn0.eyJqdGkiOiI4YmM0ODhiYy03ZjMxLTQyYTEtOWIxNS1iM2U2MDBkNTVlOTciLCJleHAiOjE1NzI4MDIwMjUsIm5iZiI6MCwiaWF0IjoxNTcyNzk0ODI1LCJpc3MiOiJodHRwOi8vNDYuMTcuNDUuMTcwOjgwODAvYXV0aC9yZWFsbXMvdW5pdmVyc2l0eS1wcm9qZWN0IiwiYXVkIjoiYWNjb3VudCIsInN1YiI6Ijk3ZDVlNTRhLWIyNWUtNDAyYy05N2Q0LWIxZTA4YmM5NGI2ZiIsInR5cCI6IkJlYXJlciIsImF6cCI6InVpLXNlcnZpY2UiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiI5ZjU1NGM1My03N2Q2LTQ3NDItODlhOS0yNDg4ZDllZjQ1OTQiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbImh0dHA6Ly9sb2NhbGhvc3Q6ODg4OCJdLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsiUk9MRV9VU0VSIiwib2ZmbGluZV9hY2Nlc3MiLCJ1bWFfYXV0aG9yaXphdGlvbiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInNjb3BlIjoicHJvZmlsZSBlbWFpbCIsImFjY291bnRfaWQiOiJiY2Q4ZmU5Ni1hOWI5LTQyYjYtODQxNC04YTRlNjFhNTQ5YzciLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsInByZWZlcnJlZF91c2VybmFtZSI6InRlc3Q1MjY5OTc2NDk0NDI3MDI3ODA0dXNlciIsImVtYWlsIjoidGVzdDUyNjk5NzY0OTQ0MjcwMjc4MDRAdGVzdC5ydSJ9.WTFY0yI4wTDSA0v3u3J75VG4-ysFeJDRcIRfh99kcQG-UGDG91doZp9BnF03oT9_9l6EMz7iAxGn2DFrVhT1dNgH6Y2fl2aXKRcrvZez83GKqONCyUGRVAPQmBNfSfc1njB4EmVZyL-MfTVc6LSU8oDMfJjBDFxTKEVi7701J-fryVGmYq3G8dFp7RCxIxGDZDIFfifPzbwbs1O-irnpcoBJkoU-4HaxWRq4X-aOSlZ25K4yh3A3VJecTS4TBB1Vc0WaY607XloEmUv2Ef6tNt7ySvcRaO0cIImxpRsdyFwLRWdx58XHqf7Oh3aXwGw-XjGYm7eWDhSIChTQNNkcLw",
		"Content-Type":  "application/json",
	}
	body, err := helper.HttpRequest(headers, bodyReq, consts.GetActivitiesForPeriodUrl, "GET")

	if err != nil {
		return nil, errors.New("GetActivitiesForPeriod CheckResponse failed")
	}

	res := make([]Activity, 0)
	err = json.Unmarshal(body, &res)
	if err != nil {
		log.Printf("Error while unmarshal GetActivitiesForPeriod: %v", err)
		return nil, err
	}
	return &res, nil
}

//!
func GetTodayActivities(c *resty.Client, token string) (*[]Activity, error) {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		Get(consts.GetTodayActivitiesUrl)

	if !helper.CheckResponse(resp, err) {
		return nil, errors.New("GetTodayActivities CheckResponse failed")
	}

	res := make([]Activity, 0)
	err = json.Unmarshal(resp.Body(), &res)
	if err != nil {
		log.Printf("Error while unmarshal GetTodayActivities: %v", err)
		return nil, err
	}
	return &res, nil
}

//!
func (a *Activity) TagActivity(c *resty.Client, token, tags string) error {
	resp, err := c.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Authorization", "Bearer "+token).
		SetBody(tags).
		SetPathParams(map[string]string{"id": a.Id}).
		Post(consts.CalendarServiceUrl + "{id}" + "/tags")

	if !helper.CheckResponse(resp, err) {
		return errors.New("TagActivity CheckResponse failed")
	}
	return nil
}

//!
func (a *Activity) DeleteActivity(c *resty.Client, token string) error {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		SetQueryParam("activityId", a.Id).
		Delete(consts.DeleteActivityUrl)

	if !helper.CheckResponse(resp, err) {
		return errors.New("DeleteActivity CheckResponse failed")
	}
	return nil
}

//???
func (a *Activity) UpdateActivity(c *resty.Client, token string) error {
	formData := []helper.FormData{
		{Key: "activityTime", Value: a.ActivityTime},
		{Key: "description", Value: a.Description},
		{Key: "duration.hours", Value: strconv.Itoa(a.Duration.Hours)},
		{Key: "subject", Value: a.Subject},
		{Key: "repeated", Value: strconv.FormatBool(a.Repeated)},
		{Key: "repeatInterval.days", Value: strconv.Itoa(a.RepeatInterval.Days)},
	}
	if len(a.Participants) > 0 {
		if len(a.Participants[0].AccountId) > 0 {
			p := helper.FormData{
				Key:   "participants[0].accountId",
				Value: a.Participants[0].AccountId,
			}
			formData = append(formData, p)
		}
		if len(a.Participants[0].GroupId) > 0 {
			p := helper.FormData{
				Key:   "participants[0].groupId",
				Value: a.Participants[0].GroupId,
			}
			formData = append(formData, p)
		}
	}

	url := consts.UpdateActivityUrl
	resp, err := helper.DoPostFormData(url, &formData, token)
	if !helper.CheckRawResponse(resp, err) {
		return errors.New("CreateActivity CheckResponse failed")
	}
	byteNum := int64(1000000)
	if resp.ContentLength > 0 {
		byteNum = resp.ContentLength
	}
	b := make([]byte, byteNum)

	n, err := resp.Body.Read(b)
	if err != nil {
		log.Printf("Error while read boby: %v", err)
		return err
	}
	if resp.ContentLength == -1 {
		b = b[0:n]
	}
	err = json.Unmarshal(b, a)
	if err != nil {
		log.Printf("Error while unmarshal CreateActivity: %v", err)
		return err
	}
	return nil
}

//!!!???
func (a *Activity) ActivityCheckIn(c *resty.Client, token string) error {
	return nil
}
