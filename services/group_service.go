package services

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-resty/resty"
	"gitlab.com/NikolayZhurbin/go_rest/consts"
	"gitlab.com/NikolayZhurbin/go_rest/helper"
	"log"
)

type GroupInfo struct {
	Closed      bool   `json:"closed,omitempty"`
	Description string `json:"description,omitempty"`
	GroupName   string `json:"groupName,omitempty"`
	Id          string `json:"id,omitempty"`
	ScreenName  string `json:"screenName,omitempty"`
}

func CreateGroupInfo(Closed bool, Description, GroupName, ScreenName string) *GroupInfo {
	r := GroupInfo{
		Id:          "",
		Closed:      Closed,
		Description: Description,
		GroupName:   GroupName,
		ScreenName:  ScreenName,
	}
	return &r
}

//Get All Group
func GetAllGroups(c *resty.Client, token string) (*[]GroupInfo, error) {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		Get(consts.UserServiceGroupUrl)

	if !helper.CheckResponse(resp, err) {
		return nil, errors.New("GetAllGroups CheckResponse failed")
	}

	g := make([]GroupInfo, 0)
	err = json.Unmarshal(resp.Body(), &g)
	if err != nil {
		log.Printf("Can't deserislize: %v\n", resp.Body())
		return nil, err
	}

	return &g, nil
}

//Create Group
func (g *GroupInfo) CreateGroup(c *resty.Client, token string) error {

	body, err := json.Marshal(*g)
	if err != nil {
		log.Printf("Error while Marshal: %v", err)
		return err
	}
	resp, err := c.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Authorization", "Bearer "+token).
		SetBody(body).
		Post(consts.UserServiceGroupUrl)

	if !helper.CheckResponse(resp, err) {
		return errors.New("CreateGroup CheckResponse failed")
	}

	err = json.Unmarshal(resp.Body(), g)
	if err != nil {
		log.Printf("Error while Unmarshal: %v", err)
		return err
	}

	return nil
}

//getGroup
func (g *GroupInfo) GetGroupById(c *resty.Client, token, id string) error {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"id": id}).
		Get(consts.UserServiceGroupUrl + "{id}")

	if !helper.CheckResponse(resp, err) {
		return errors.New("GetGroupById CheckResponse failed")
	}

	err = json.Unmarshal(resp.Body(), g)
	if err != nil {
		log.Printf("Can't deserislize: %v\n", resp.Body())
		return err
	}

	return nil
}

//!getUsersByGroupId
func (g *GroupInfo) GetUsersInGroupById(c *resty.Client, token, id string) (*[]User, error) {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"id": id}).
		Get(consts.UserServiceGroupUrl + "{id}/users")

	if !helper.CheckResponse(resp, err) {
		return nil, errors.New("GetUsersInGroupById CheckResponse failed")
	}

	users := make([]User, 0)
	err = json.Unmarshal(resp.Body(), &users)
	if err != nil {
		log.Printf("Can't deserislize: %v\n", resp.Body())
		return nil, err
	}

	return &users, nil
}

//Delete Group by Id
func (g *GroupInfo) DeleteGroupById(c *resty.Client, token string) error {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"id": g.Id}).
		Delete(consts.UserServiceGroupUrl + "{id}")

	if !helper.CheckResponse(resp, err) {
		return errors.New("DeleteGroupById CheckResponse failed")
	}
	return nil
}

//Tag Group
func (g *GroupInfo) TagGroup(c *resty.Client, token, tags string) error {
	resp, err := c.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Authorization", "Bearer "+token).
		SetBody(tags).
		SetPathParams(map[string]string{"Id": g.Id}).
		Post(consts.UserServiceGroupUrl + "{Id}" + "/Tags")

	if !helper.CheckResponse(resp, err) {
		return errors.New("TagGroup CheckResponse failed")
	}
	return nil
}

func (g *GroupInfo) PrintGroup() {
	fmt.Println("Printing Group Info")
	fmt.Printf("Id: %v\n", g.Id)
	fmt.Printf("GroupName: %v\n", g.GroupName)
	fmt.Printf("ScreenName: %v\n", g.ScreenName)
	fmt.Printf("Description: %v\n", g.Description)
	fmt.Printf("Closed: %v\n", g.Closed)
	fmt.Println("-------------------------------------------------------------------------------")
}
func PrintAllGroups(groups []GroupInfo) {
	for _, g := range groups {
		g.PrintGroup()
	}
}
