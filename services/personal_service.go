package services

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-resty/resty"
	"gitlab.com/NikolayZhurbin/go_rest/consts"
	"gitlab.com/NikolayZhurbin/go_rest/helper"
	"log"
)

type Personal struct {
	AccountId  string `json:"accountId,omitempty"`
	Email      string `json:"email,omitempty"`
	FirstName  string `json:"firstName,omitempty"`
	LastName   string `json:"lastName,omitempty"`
	MiddleName string `json:"middleName,omitempty"`
	Phone      string `json:"phone,omitempty"`
}

func (p *Personal) GetPersonalInfo(c *resty.Client, token, accountId string) error {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"accountId": accountId}).
		Get(consts.PersonalInfoServiceUrl + "{accountId}")

	if !helper.CheckResponse(resp, err) {
		return errors.New("GetPersonalInfo CheckResponse failed")
	}

	err = json.Unmarshal(resp.Body(), p)
	if err != nil {
		log.Printf("Can't deserislize: %v\n", resp.Body())
		return err
	}

	return nil
}

func (p *Personal) PutPersonalInfo(c *resty.Client, token string) error {
	body, err := json.Marshal(*p)
	if err != nil {
		log.Printf("Error while Marshal: %v", err)
		return err
	}
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		SetHeader("Content-Type", "application/json").
		SetBody(body).
		Put(consts.PersonalInfoServiceUrl)

	if !helper.CheckResponse(resp, err) {
		return errors.New("PutPersonalInfo CheckResponse failed")
	}
	return nil
}

func (p *Personal) PrintPersonalInfo() {
	fmt.Println("Printing Personal Info")
	fmt.Printf("AccountId: %v\n", p.AccountId)
	fmt.Printf("FirstName: %v\n", p.FirstName)
	fmt.Printf("LastName: %v\n", p.LastName)
	fmt.Printf("MiddleName: %v\n", p.MiddleName)
	fmt.Printf("Phone: %v\n", p.Phone)
	fmt.Printf("Email: %v\n", p.Email)
	fmt.Println("-------------------------------------------------------------------------------------------------")
}
