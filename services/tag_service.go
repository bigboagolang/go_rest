package services

import (
	"../consts"
	"../domain"
	"../helper"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-resty/resty"
	"log"
)

//!findAllTags
func GetAllTags(c *resty.Client, token string) (*Tags, error) {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		Get(consts.TagServiceUrl)

	if !helper.CheckResponse(resp, err) {
		return nil, errors.New("GetAllTags CheckResponse failed")
	}

	res := make(domain.Tags, 0)
	err = json.Unmarshal(resp.Body(), &res)
	if err != nil {
		log.Printf("Error while unmarshal GetAllTags: %v", err)
		return nil, err
	}
	return &res, nil
}

//!findById
func (t *domain.Tag) GetTagById(c *resty.Client, token string) error {
	fmt.Printf("Tag Id to get: %v\n", t.Id)
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"id": t.Id}).
		Get(consts.TagServiceUrl + "{id}")

	if !helper.CheckResponse(resp, err) {
		return errors.New("GetTagById CheckResponse failed")
	}
	err = json.Unmarshal(resp.Body(), t)
	if err != nil {
		log.Printf("Can't deserislize: %v\n", string(resp.Body()))
		return err
	}
	return nil
}

//!createNewTag
func (t *Tags) CreateTag(c *resty.Client, token string) error {
	body := "["
	for _, v := range *t {
		body += "\"" + v.Value + "\""
	}
	body += "]"
	fmt.Printf("Tag body: %v\n", string(body))
	resp, err := c.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Authorization", "Bearer "+token).
		SetBody(body).
		Post(consts.TagServiceUrl)

	if !helper.CheckResponse(resp, err) {
		return errors.New("CreateTag CheckResponse failed")
	}
	err = json.Unmarshal(resp.Body(), t)
	if err != nil {
		log.Printf("Can't deserislize: %v\n", string(resp.Body()))
		return err
	}
	return nil
}

//!deleteById
func (t *Tag) DeleteTagById(c *resty.Client, token string) error {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"id": t.Id}).
		Delete(consts.TagServiceUrl + "{id}")

	if !helper.CheckResponse(resp, err) {
		return errors.New("GetTagById CheckResponse failed")
	}

	return nil
}

func (t *Tag) Print() {
	fmt.Println("Printing Tag")
	fmt.Printf("ID: %v\n", t.Id)
	fmt.Printf("Value: %v\n", t.Value)
	fmt.Println("-----------------------------------------------------------------")
}
