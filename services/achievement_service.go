package services

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-resty/resty"
	"gitlab.com/NikolayZhurbin/go_rest/consts"
	"gitlab.com/NikolayZhurbin/go_rest/helper"
	"log"
	"strconv"
)

type internalComment struct {
	Text    string `json:"text,omitempty"`
	ReplyTo string `json:"replyTo,omitempty"`
}
type AchievementComment struct {
	Id          string `json:"id,omitempty"`
	Comment     string `json:"text,omitempty"`
	ReplyTo     string `json:"replyTo,omitempty"`
	CreatedDate string `json:"createdDate,omitempty"`
}
type Comments []AchievementComment

type Achievement struct {
	Attachment        string `json:"attachment,omitempty"`
	Comments          `json:"comments,omitempty"`
	Description       string `json:"description,omitempty"`
	Id                string `json:"id,omitempty,omitempty"`
	Name              string `json:"name,omitempty"`
	ParticipantsCount int    `json:"participantsNumber,omitempty"`
	Tags              `json:"tags,omitempty"`
	AchievementType   `json:"type,omitempty"`
	Date              string `json:"eventDate,omitempty"`
}

//!
func GetAchievements(c *resty.Client, token string) (*[]Achievement, error) {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		Get(consts.AchievementServiceUrl)

	if !helper.CheckResponse(resp, err) {
		return nil, errors.New("GetAchievements CheckResponse failed")
	}
	g := make([]Achievement, 0)
	err = json.Unmarshal(resp.Body(), &g)
	if err != nil {
		log.Printf("Can't deserislize: %v\n", resp.Body())
		return nil, err
	}

	return &g, nil
}

//!
func (a *Achievement) Print() {
	fmt.Println("-----Printing Achievement-----")
	fmt.Printf("Id: %v\n", a.Id)
	fmt.Printf("Name: %v\n", a.Name)
	fmt.Printf("Type: %v\n", a.AchievementType.Value)
	fmt.Printf("Description: %v\n", a.Description)
	fmt.Printf("Attachment: %v\n", a.Attachment)
	fmt.Printf("ParticipantsCount: %v\n", a.ParticipantsCount)
	fmt.Printf("Date: %v\n", a.Date)
	fmt.Println("-----Printing Comments--------")
	for _, v := range a.Comments {
		fmt.Printf("CommentId: %v\n", v.Id)
		fmt.Printf("Comment: %v\n", v.Comment)
		fmt.Printf("ReplyTo: %v\n", v.ReplyTo)
		fmt.Printf("CreatedDate: %v\n", v.CreatedDate)
	}
	fmt.Println("-----Printing Tags------------")
	for _, v := range a.Tags {
		fmt.Printf("TagId: %v\n", v.Id)
		fmt.Printf("Tag: %v\n", v.Value)
	}
	fmt.Println("------------------------------")
}

//!
func (a *Achievement) CreateAchievement(token string) error {
	formData := []helper.FormData{
		{Key: "description", Value: a.Description},
		{Key: "name", Value: a.Name},
		{Key: "participantsNumber", Value: strconv.Itoa(a.ParticipantsCount)},
		{Key: "eventDate", Value: a.Date},
	}
	url := consts.AchievementServiceUrl
	resp, err := helper.DoPostFormData(url, &formData, token)
	if !helper.CheckRawResponse(resp, err) {
		return errors.New("CreateAchievement CheckResponse failed")
	}
	p := make([]byte, 1000)
	count, err := resp.Body.Read(p)
	g := p[0:count]
	err = json.Unmarshal(g, a)
	if err != nil {
		log.Printf("Can't deserislize: %v\n", string(p))
		return err
	}

	return nil
}

//!
func (a *Achievement) DeleteAchievement(c *resty.Client, token string) error {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"id": a.Id}).
		Delete(consts.AchievementServiceUrl + "{id}")

	if !helper.CheckResponse(resp, err) {
		return errors.New("DeleteAchievement CheckResponse failed")
	}
	return nil
}

//!PATCH
func (a *Achievement) UpdateAchievement(c *resty.Client, token string) error {
	formData := []helper.FormData{
		{Key: "description", Value: a.Description},
		{Key: "name", Value: a.Name},
		{Key: "participantsNumber", Value: strconv.Itoa(a.ParticipantsCount)},
		{Key: "eventDate", Value: a.Date},
	}
	url := consts.AchievementServiceUrl + a.Id
	resp, err := helper.DoPatchFormData(url, &formData, token)
	if !helper.CheckRawResponse(resp, err) {
		return errors.New("UpdateAchievement CheckResponse failed")
	}

	return nil
}

func PrintAllAchievements(a *[]Achievement) {
	for _, v := range *a {
		v.Print()
	}
}

//!
func GetCommentsById(c *resty.Client, token, aid string) (comm *[]Comment, err error) {
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"id": aid}).
		Get(consts.AchievementCommentsServiceUrl)

	if !helper.CheckResponse(resp, err) {
		return nil, errors.New("GetComments CheckResponse failed")
	}
	g := make([]Comment, 0)
	err = json.Unmarshal(resp.Body(), &g)
	if err != nil {
		log.Printf("GetCommentsById: Can't deserislize: %v\n", string(resp.Body()))
		return nil, err
	}

	return &g, nil
}

//!
func (comm *AchievementComment) CreateComment(c *resty.Client, token, tid string) error {
	tmp := internalComment{
		Text: comm.Comment,
	}

	body, err := json.Marshal(tmp)
	fmt.Println(string(body))
	if err != nil {
		log.Printf("Error while Marshal CreateComment: %v", err)
		return nil
	}

	resp, err := c.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"id": tid}).
		SetBody(body).
		Post(consts.AchievementCommentsServiceUrl)

	if !helper.CheckResponse(resp, err) {
		return errors.New("CreateComment CheckResponse failed")
	}

	return nil

}

func PrintAllComments(c *[]Comment) {
	for _, v := range *c {
		v.PrintComment()
	}
}

func (comm *AchievementComment) PrintComment() {
	fmt.Println("-----Printing Comments--------")
	fmt.Printf("CommentId: %v\n", comm.Id)
	fmt.Printf("Comment: %v\n", comm.Comment)
	fmt.Printf("ReplyTo: %v\n", comm.ReplyTo)
	fmt.Printf("CreatedDate: %v\n", comm.CreatedDate)
	fmt.Println("------------------------------")
}

//tags should be form as ["tag1", "tag2", ...]
func (a *Achievement) TagAchievement(c *resty.Client, token, tags string) error {
	resp, err := c.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Authorization", "Bearer "+token).
		SetPathParams(map[string]string{"id": a.Id}).
		SetBody(tags).
		Post(consts.AchievementTagServiceUrl)

	if !helper.CheckResponse(resp, err) {
		return errors.New("CreateComment CheckResponse failed")
	}
	return nil
}
