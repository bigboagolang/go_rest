package main

import (
	"github.com/go-resty/resty"
	"gitlab.com/NikolayZhurbin/go_rest/activity"
	"gitlab.com/NikolayZhurbin/go_rest/consts"
	"gitlab.com/NikolayZhurbin/go_rest/token"
	user_service "gitlab.com/NikolayZhurbin/go_rest/user-service"
	"log"
)

func main() {

	//client init
	consts.Client = resty.New()
	log.SetFlags(log.Lshortfile | log.LstdFlags)
	var err error

	r := new(token.AccessToken)
	err = r.GetAccessToken(consts.Client, "test8607586426160161405User", "1234")
	if err != nil {
		log.Printf("GetAccessToken - Error: %v", err)
	}
	log.Printf("Token: %v", r.Token)
	//Get User by Auth TokenInfo
	uInfo := new(user_service.UserInfo)
	err = uInfo.GetUserByToken(consts.Client, r.Token)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	uInfo.PrintUserInfo()

	//Activity
	//	del := activity.Activity{
	//		Id:                "ae9d25276e23597b016e2842faac001d",
	//	}
	//	err = del.DeleteActivity(consts.Client, r.Token)
	//	if err != nil {
	//		log.Fatalf("Error: %v", err)
	//	}
	//	act, err := activity.GetAllActivities(consts.Client, r.Token)
	//	if err != nil {
	//		log.Fatalf("Error: %v", err)
	//	}
	//	for _, v := range *act {
	//		v.Subject = "Updated subject 001"
	//		_ = v.UpdateActivity(consts.Client, r.Token)
	//	}
	//	act, err := activity.GetAllActivities(consts.Client, r.Token)
	//	if err != nil {
	//		log.Fatalf("Error: %v", err)
	//	}
	//	for _, v := range *act {
	//		_ = v.TagActivity(consts.Client, r.Token, "[\"elk001\", \"elk002\", \"elk003\"]")
	//	}
	//	act, err = activity.GetAllActivities(consts.Client, r.Token)
	//	if err != nil {
	//		log.Fatalf("Error: %v", err)
	//	}
	//	act, err := activity.GetActivitiesForPeriod(consts.Client, r.Token, "2019-11-01", "2019-12-01")
	//	if err != nil {
	//		log.Fatalf("Error: %v", err)
	//	}
	//	activity.PrintAllActivities(act, false)
	//
	//	act, err := activity.GetTodayActivities(consts.Client, r.Token)
	//	if err != nil {
	//		log.Fatalf("Error: %v", err)
	//	}
	//	activity.PrintAllActivities(act, false)
	//	err = user_service.CreateActivityWithTagToGroup(r, "77777", "[\"elk001\", \"elk002\", \"elk003\"]", "d204c749-30d5-4b6d-9fc6-b68e517eb6ff")
	//	if err != nil {
	//		log.Fatalf("Error: %v", err)
	//	}
	act, err := activity.GetAllActivities(consts.Client, r.Token)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	activity.PrintAllActivities(act, true)

	//test_cases.CreateNUsersWithSkillsInOneGroup(5, true, "[\"elk003\", \"elk004\"]")
	//test_cases.TaskE2EInit(true)
	//err = test_cases.DistributeNewTaskToNewGroupWithTags(5, true, "[\"elk001\", \"elk002\", \"elk003\"]")
	//if err != nil {
	//	fmt.Println(err)
	//}

	/*	r := new(token.AccessToken)
		err := r.GetAccessToken(consts.Client, "boa22", "1234")
		if err != nil {
			log.Printf("GetAccessToken - Error: %v", err)
		}
		log.Printf("Token: %v", r.Token)
		//Get User by Auth TokenInfo
		uInfo := new(user_service.UserInfo)
		err = uInfo.GetUserByToken(consts.Client, r.Token)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}
		uInfo.PrintUserInfo()*/

	/*
		s := rand.NewSource(time.Now().UnixNano())
		rNum := rand.New(s)
		num := strconv.Itoa(rNum.Int())
		t1, err := test_cases.CreateUserAndGetToken(
			"test" + num + "@test.ru",
			"First" + num + "Name",
			"Last" + num + "Name",
			"Middle" + num + "Name",
			"1234",
			"test" + num + "User",
			"1234",
		)
		if err != nil {
			log.Fatalln(err)
		}
		t1.PrintAccessToken()
		num = strconv.Itoa(rNum.Int())
		t2, err := test_cases.CreateUserAndGetToken(
			"test" + num + "@test.ru",
			"First" + num + "Name",
			"Last" + num + "Name",
			"Middle" + num + "Name",
			"1234",
			"test" + num + "User",
			"1234",
		)
		if err != nil {
			log.Fatalln(err)
		}
		t2.PrintAccessToken()

		//Get User by Auth TokenInfo
		uInfo := new(user_service.UserInfo)
		err = uInfo.GetUserByToken(consts.Client, t1.Token)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}
		uInfo.PrintUserInfo()

		gInfo := user_service.CreateGroupInfo(false, "Task Group 02", "Task Group 02", "Task Group 02")
		err = gInfo.CreateGroup(consts.Client, t1.Token)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}
		err = uInfo.AddGroupsToUserByAccountId(consts.Client, t1.Token, uInfo.AccountId, gInfo.Id)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}
		err = uInfo.GetUserByToken(consts.Client, t2.Token)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}
		uInfo.PrintUserInfo()
		err = uInfo.AddGroupsToUserByAccountId(consts.Client, t2.Token, uInfo.AccountId, gInfo.Id)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}

		allUsers, err := gInfo.GetUsersInGroupById(consts.Client, t2.Token, gInfo.Id)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}
		fmt.Println("-------------------Printing All Users in Group 8151c674-cf8b-408b-be90-978afc64f8e9-------------------")
		for _, u := range *allUsers {
			u.PrintUser()
		}
		fmt.Printf("GroupID: %v", gInfo.Id)
	*/
	//---User
	/*
		//Get User by Auth TokenInfo
		uInfo := new(user_service.UserInfo)
		err = uInfo.GetUserByToken(client, r.Token)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}
		uInfo.PrintUserInfo()

		//Update User by Auth TokenInfo
		uInfo.UserName = "boa22Updated"
		err = uInfo.UpdateUserByToken(client, r.Token)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}
		uInfo.PrintUserInfo()

		//Get User by accountId
		err = uInfo.GetUserByAccountId(client, r.Token, uInfo.AccountId)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}
		uInfo.PrintUserInfo()

		//Update User by accountId
		uInfo.UserName = "boa22"
		err = uInfo.UpdateUserByAccountId(client, r.Token, uInfo.AccountId)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}
		uInfo.PrintUserInfo()
	*/
	//Groups to User
	/*
	   //adding current user to the groups: 8151c674-cf8b-408b-be90-978afc64f8e9,ab078472-fb2b-48df-aec1-598d0cc13b19
	   //Add Group to user
	   	err = uInfo.AddGroupsToUserByAccountId(client, r.Token, uInfo.AccountId, "8151c674-cf8b-408b-be90-978afc64f8e9,ab078472-fb2b-48df-aec1-598d0cc13b19")
	   	if err != nil {
	   		log.Fatalf("Error: %v", err)
	   	}
	   	fmt.Println("AddGroupsToUserByAccountId is OK")

	   //Delete user from Group
	   	err = uInfo.DeleteUserFromGroupById(client, r.Token, "8151c674-cf8b-408b-be90-978afc64f8e9")
	   	if err != nil {
	   		log.Fatalf("Error: %v", err)
	   	}
	   	fmt.Println("DeleteUserFromGroupById is OK")

	   //Add Group to user by Auth Token Info
	   	err = uInfo.AddGroupsToUserByToken(client, r.Token, "8151c674-cf8b-408b-be90-978afc64f8e9")
	   	if err != nil {
	   		log.Fatalf("Error: %v", err)
	   	}
	   	fmt.Println("AddGroupsToUserByToken is OK")

	   	gInfo := new(user_service.GroupInfo)
	   //getUsersByGroupId
	   	allUsers, err := gInfo.GetUsersInGroupById(client, r.Token, "8151c674-cf8b-408b-be90-978afc64f8e9")
	   	if err != nil {
	   		log.Fatalf("Error: %v", err)
	   	}
	   	fmt.Println("-------------------Printing All Users in Group 8151c674-cf8b-408b-be90-978afc64f8e9-------------------")
	   	for _, u := range *allUsers {
	   		u.PrintUser()
	   	}
	   	fmt.Println("GetUsersInGroupById is OK")
	   //Get Groups for user by AccountId
	   	allGroups, err := uInfo.GetGroupsByAccountId(client, r.Token, uInfo.AccountId)
	   	if err != nil {
	   		log.Fatalf("Error: %v", err)
	   	}
	   	fmt.Println("-------------------Printing GetGroupsByAccountId for User-------------------")
	   	for _, g := range *allGroups {
	   		g.PrintGroup()
	   	}
	   	fmt.Println("GetGroupsByAccountId is OK")
	   	fmt.Println("--------------------------------------------------------------")

	   //Get Groups for user by Auth TokenInfo
	   	allGroups, err = uInfo.GetGroupsByToken(client, r.Token)
	   	if err != nil {
	   		log.Fatalf("Error: %v", err)
	   	}
	   	fmt.Println("-------------------Printing GetGroupsByToken for User-------------------")
	   	for _, g := range *allGroups {
	   		g.PrintGroup()
	   	}
	   	fmt.Println("GetGroupsByToken is OK")
	   	fmt.Println("--------------------------------------------------------------")
	*/
	//---Skills To User
	/*
	   //Add Skills to user By AccountId      7fd58685-cd85-43ff-8648-5f41eab0b1d1,8ddfef06-7cdf-4934-b861-e2fe34855109
	   	err = uInfo.AddSkillsToUserByAccountId(client, r.Token, uInfo.AccountId,"7fd58685-cd85-43ff-8648-5f41eab0b1d1,8ddfef06-7cdf-4934-b861-e2fe34855109")
	   	if err != nil {
	   		log.Fatalf("Error: %v", err)
	   	}
	   //Add Skills to user by Auth Token Info     8ddfef06-7cdf-4934-b861-e2fe34855109
	   	err = uInfo.AddSkillsToUserByToken(client, r.Token, "8ddfef06-7cdf-4934-b861-e2fe34855109")
	   	if err != nil {
	   		log.Fatalf("Error: %v", err)
	   	}

	   	//Get UsersSkills by Auth Token Info
	   	allS, err := uInfo.GetSkillsByToken(client, r.Token)
	   	if err != nil {
	   		log.Fatalf("Error: %v", err)
	   	}
	   	//fmt.Println(allS)
	   	for _, s := range *allS {
	   		s.PrintUserSkills()
	   	}
	   	fmt.Println("")
	   	fmt.Println("----------------------------------------------------------------------")
	   	fmt.Println("")

	   //Delete UserSkill ByAccountId

	   	err = uInfo.DeleteUserSkillByAccountId(client, r.Token, uInfo.AccountId,"7fd58685-cd85-43ff-8648-5f41eab0b1d1")
	   	if err != nil {
	   		log.Fatalf("Error: %v", err)
	   	}
	   //Delete UserSkill by Auth Token Info
	   	err = uInfo.DeleteUserSkillByToken(client, r.Token, "8ddfef06-7cdf-4934-b861-e2fe34855109")
	   		if err != nil {
	   		log.Fatalf("Error: %v", err)
	   	}
	*/
	//confirm Skill
	/*
		//boa11 - fc51e6e6-9801-44c1-9f2e-e647977fc225
		//SkillName: skill for test 212: 9de710e2-0ac1-4ca2-8cb2-e7c4e70227d3
		t := new(token.AccessToken)
		err = t.GetAccessToken(client, "boa11", "1234")
		err = uInfo.ConfirmSkill(client, r.Token, "7fd58685-cd85-43ff-8648-5f41eab0b1d1", "fc51e6e6-9801-44c1-9f2e-e647977fc225")
		fmt.Println(t.Token)
		//Get UsersSkills by Auth Token Info
		allS, err = uInfo.GetSkillsByToken(client, t.Token)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}
		for _, s := range *allS {
			s.PrintUserSkills()
		}
	*/

	//getting Personal Info by token
	/*
		pInfo := new(person_controller.Personal)
		err = pInfo.GetPersonalInfo(consts.Client, t2.Token, uInfo.AccountId)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}
		pInfo.PrintPersonalInfo()
	*/
	//---Groups
	/*

		gInfo := user_service.CreateGroupInfo(false,"Group Description", "GroupName01", "ScreenName01")
		err = gInfo.CreateGroup(client, r.Token)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}
		gInfo.PrintGroup()

		//Getting group ab078472-fb2b-48df-aec1-598d0cc13b19
		err = gInfo.GetGroupById(client, r.Token, "ab078472-fb2b-48df-aec1-598d0cc13b19")
		if err != nil {
			log.Fatalf("Error: %v", err)
		}
		gInfo.PrintGroup()

		allUsers, err := gInfo.GetUsersInGroupById(client, r.Token, "ab078472-fb2b-48df-aec1-598d0cc13b19")
		if err != nil {
			log.Fatalf("Error: %v", err)
		}
		fmt.Println("-------------------Printing All Users in Group ab078472-fb2b-48df-aec1-598d0cc13b19-------------------")
		for _, u := range *allUsers {
			u.PrintUser()
		}
		gInfo = user_service.CreateGroupInfo(false,"ToDeleteGroupName", "ToDeleteGroupName", "ToDeleteGroupName")
		err = gInfo.CreateGroup(client, r.Token)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}
		DelId := gInfo.Id

		allGroups, err := user_service.GetAllGroups(client, r.Token)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}
		fmt.Println("-------------------Printing All Groups-------------------")
		for _, g := range *allGroups {
			g.PrintGroup()
		}

		err = gInfo.DeleteGroupById(client, r.Token)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}

		err = gInfo.GetGroupById(client, r.Token, DelId)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}

	*/
	//---Personal Info
	/*
		//updating Personal Info by token
		pInfo.FirstName = "Nik1Put2"
		err = pInfo.PutPersonalInfo(client, r.Token)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}

		//getting Personal Info by token
		pInfo = new(person_controller.Personal)
		err = pInfo.GetPersonalInfo(client, r.Token, uInfo.AccountId)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}
		pInfo.PrintPersonalInfo()



		err = u.DeleteUser(client, uInfo.AccountId, r.Token)
		if err != nil {
			log.Printf("Error: %v", err)
		}

		//getting Personal Info by token
		pInfo := new(person_controller.Personal)
		err = pInfo.GetPersonalInfo(client, r.Token, uInfo.AccountId)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}
		pInfo.PrintPersonalInfo()

	*/
	//---Achievement Types
	/*
	   //Printing AchievementType
	   //ID: 893c0cba-ecdf-43c1-8f61-a5f234c9615b
	   //Value: {"value":"test002"}
	   	//get Achievement Types
	   	achTypeList, err := achievement_service.GetAchievementTypeList(consts.Client, r.Token)
	   	if err != nil {
	   		log.Fatalf("Error during getting all Achievement Types: %v\n", err)
	   	}
	   	for _, achType := range *achTypeList {
	   		achType.Print()
	   	}
	*/
	//CreateAchievementType Achievement Type
	/*
		achType := achievement_service.AchievementType{
			Value: "testToDelete777",
		}
		err = achType.CreateAchievementType(consts.Client, r.Token)
		if err != nil {
			log.Fatalf("Error while creating AchievementType: %v", err)
		}
		fmt.Println("new type")
		achType.Print()

		err = achType.DeleteAchievementType(consts.Client, r.Token)
		if err != nil {
			log.Fatalf("Error while DeleteAchievementType: %v", err)
		}

		//get Achievement Types
		achTypeList, err := achievement_service.GetAchievementTypeList(consts.Client, r.Token)
		if err != nil {
			log.Fatalf("Error during getting all Achievement Types: %v\n", err)
		}
		for _, achType := range *achTypeList {
			achType.Print()
		}
	*/
	//---Achievement
	/*	//42900b98-0d20-4b6e-9d6c-0175b19e7cae
		ach := achievement_service.Achievement{
			Id:                "42900b98-0d20-4b6e-9d6c-0175b19e7cae",
			Name:              "Updated Achievement with comments",
			ParticipantsCount: 170,
			Description:       "Updated description3",
			Date:              "2019-10-30",
		}

		err = ach.TagAchievement(consts.Client, r.Token, "[\"TagAchievement01\", \"TagAchievement02\"]")
		if err != nil {
			log.Fatalf("Error: %v", err)
		}
		err = ach.UpdateAchievement(consts.Client, r.Token)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}
		allAch, err := achievement_service.GetAchievements(consts.Client, r.Token)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}
		achievement_service.PrintAllAchievements(allAch)
		   	ach := achievement_service.Achievement{
				Description:       "Description",
				Name:              "Achievement with comments",
				ParticipantsCount: 110,
				Date:              "2019-10-31",
			}

			allAch, err := achievement_service.GetAchievements(consts.Client, r.Token)
			if err != nil {
				log.Fatalf("Error: %v", err)
			}
			achievement_service.PrintAllAchievements(allAch)

			err = ach.CreateAchievement(r.Token)
		   	if err != nil {
		   		log.Fatalf("Error: %v", err)
		   	}
		   	ach.Print()

		   	ach = achievement_service.Achievement{
			Id:                "64675a7a-7964-4573-a263-c43e4ab835e4",
			}
		   	err = ach.DeleteAchievement(consts.Client, r.Token)
		   	if err != nil {
		   			log.Fatalf("Error: %v", err)
		   	}

		   	allAch, err := achievement_service.GetAchievements(consts.Client, r.Token)
		   	if err != nil {
		   		log.Fatalf("Error: %v", err)
		   	}
		   	achievement_service.PrintAllAchievements(allAch)
	*/
	//---AchievementComments
	//42900b98-0d20-4b6e-9d6c-0175b19e7cae
	/*
		aid := "42900b98-0d20-4b6e-9d6c-0175b19e7cae"
		allComm, err := achievement_service.GetCommentsById(consts.Client, r.Token, aid)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}
		achievement_service.PrintAllComments(allComm)

		com := achievement_service.Comment{
			Comment: "testcomment2",
		}
		err = com.CreateComment(consts.Client, r.Token, aid)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}

	*/
	//---Tags
	/*	allTags, err := tag_service.GetAllTags(consts.Client, r.Token)
		if err != nil {
			log.Fatalf("Error during getting all tags: %v", err)
		}
		for _, tag := range *allTags {
			tag.Print()
		}

		tags := make(tag_service.Tags, 0)
		tag := tag_service.Tag{
			Value: "TestTag777888",
		}
		tags = append(tags, tag)

		err = tags.CreateTag(consts.Client, r.Token)
		if err != nil {
			log.Fatalf("Error during CreateTag: %v", err)
		}
		tag.Id = "e02e2d2f-0d82-4026-a09f-7b2bd43f8fd6"
		err = tag.GetTagById(consts.Client, r.Token)
		if err != nil {
			log.Fatalf("Error during getting tag by id: %v", err)
		}
		fmt.Println("------------------GetTagById------------------")
		tag.Print()

		err = tag.DeleteTagById(consts.Client, r.Token)
		if err != nil {
			log.Fatalf("Error during DeleteTagById: %v", err)
		}

		allTags, err = tag_service.GetAllTags(consts.Client, r.Token)
		if err != nil {
			log.Fatalf("Error during getting all tags: %v", err)
		}
		for _, tag := range *allTags {
			tag.Print()
		}
	*/
	//---Skills
	/*
		//createSkill
		s := user_service.CreateSkill("Java999")
		err = s.PostSkill(client, r.Token)
		if err != nil {
			log.Fatalf("Error while creating Skill: %v", err)
		}
		s.PrintSkill()

		//delete
		err = s.DeleteSkillById(client, r.Token, s.Id)
		if err != nil {
			log.Fatalf("Error while delete Skill: %v", err)
		}

		//get skill 8ddfef06-7cdf-4934-b861-e2fe34855109
		err = s.GetSkillById(client, r.Token, "8ddfef06-7cdf-4934-b861-e2fe34855109")
		if err != nil {
			log.Fatalf("Error while getting Skill: %v", err)
		}
		s.PrintSkill()


		fmt.Println("----------------SkillPut---------------")
		s.Value = "SkillPut"
		err = s.PutSkill(client, r.Token)
		if err != nil {
			log.Fatalf("Error while Put Skill: %v", err)
		}
		s.PrintSkill()



		//getAllSkills
		aSkills, err := user_service.GetAllSkills(client, r.Token)
		if err != nil {
			log.Fatalf("Error while getting all Skill: %v", err)
		}
		user_service.PrintAllSkills(*aSkills)

	*/
	//---Priority
	/*

		pr := task_service.Priority{Value:"test808"}
		//Create new priority
		err = pr.CreatePriority(client, r.Token)
		if err != nil {
			log.Fatalf("Error during CreatePriority: %v", err)
		}
		fmt.Println("CreatePriority")
		pr.Print()
		//Delete priority by ID
		err = pr.DeletePriority(client, r.Token)
		if err != nil {
			log.Fatalf("Error during DeletePriority: %v", err)
		}
		//Get the list of available priorities
		allPr, err := task_service.GetAllPriories(client, r.Token)
		if err != nil {
			log.Fatalf("Error during getting all Priorities: %v", err)
		}
		for _, pr := range *allPr {
			pr.Print()
		}
	*/
	//---Status
	/*
		st := task_service.Status{Value:"test919"}
		//Create new status
		err = st.CreateStatus(client, r.Token)
		if err != nil {
			log.Fatalf("Error during creating new status: %v", err)
		}
		st.Print()

		//Delete priority by ID
		err = st.DeleteStatus(client, r.Token)
		if err != nil {
			log.Fatalf("Error during getting all Priorities: %v", err)
		}
		//Get the list of available statuses
		allSt, err := task_service.GetAllStatuses(client, r.Token)
		if err != nil {
			log.Fatalf("Error during getting all Priorities: %v", err)
		}
		for _, st := range *allSt {
			st.Print()
		}
	*/
	//---Tasks

	/*	allTasks, err := task_service.GetAllTasks(consts.Client, r.Token)
		if err != nil {
			log.Fatalf("Error during getting all Tasks: %v", err)
		}
		for _, ts := range *allTasks {
			ts.Print()
			fmt.Println("Printing comments for task")
			allC, err := task_service.GetAllCommentsByTaskId(consts.Client, r.Token, ts.Id)
			if err != nil {
				log.Fatalf("Error during getting all Comments: %v", err)
			}
			for _, c := range *allC {
				c.PrintComment()
			}
		}


			comment := task_service.Comment{
				AssignedTo: uInfo.AccountId,
				DueDate:    "2019-10-12",
				Priority:   task_service.Priority{Id:"ed8906d0-ea68-11e9-8124-2e6b45b29a25"},
				Status:     task_service.Status{Id:"ed7dfcb8-ea68-11e9-8124-2e6b45b29a25"},
				Subject:    "test555",
				Text:       "test555text",
			}
			err = comment.CreateComment(r.Token)
			if err != nil {
				log.Fatalf("Error during getting all Tasks: %v", err)
			}
	*/
	//tag task - 24243d4f-211b-4f08-81c4-05f367930060
	/*		task := task_service.Task{
				Id:          "24243d4f-211b-4f08-81c4-05f367930060",
			}
			err = task.TagTask(consts.Client, r.Token, "[\"tasktag01\",\"tasktag02\"]")
			if err != nil {
				log.Fatalf("Error during TagTask: %v", err)
			}
	*/

	//delete task 25d1c871-8472-4924-9cd2-e1ffc524a539
	/*	task := task_service.Task{
			Id:          "25d1c871-8472-4924-9cd2-e1ffc524a539",
		}
		err = task.DeleteTask(consts.Client, r.Token)
		if err != nil {
			log.Fatalf("Error during TagTask: %v", err)
		}*/

	/*	allTasks, err := task_service.GetAllTasks(consts.Client, r.Token)
		if err != nil {
			log.Fatalf("Error during getting all Tasks: %v", err)
		}
		for _, ts := range *allTasks {
			ts.Print()
			fmt.Println("Printing comments for task")
			allC, err := task_service.GetAllCommentsByTaskId(consts.Client, r.Token, ts.Id)
			if err != nil {
				log.Fatalf("Error during getting all Comments: %v", err)
			}
			for _, c := range *allC {
				c.PrintComment()
			}
		}*/

}
